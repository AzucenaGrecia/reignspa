<h1>Reign Challange Full-Stack</h1>

<h2>System requirements</h2>
<h3>If you will use Docker<h3>
<ul>
<li>1. Install docker Engine (click [here](https://docs.docker.com/engine/install/))</li>
<li>2. Install docker compose(click [here](https://docs.docker.com/compose/install/)) </li>
</ul>

<h3>If you will use your own OS<h3>
<ul>
<li>1. Install nodejs version v.12.20 (click [here](https://nodejs.org/en/download/))</li>
</ul>

<h2>Steps for Docker</h2>
<p> 1. Clone the repository in your local machine<p></br>
<p> 2. Open the project in VS Code or your favorite ✨ code editor</p></br>
<p> 3. Run the command:</p> </br>
<p></p><code>docker-compose up -d</code></br>

<p> 3. Run the command:</p> </br>
<p></p><code>docker-compose -f docker-compose.yml up</code></br>

<p> 4. Open a browser and type the next urls 🐱‍💻:</p></br>
<p></p><code>http://localhost:3000/</code></br>
<p></p><code>http://localhost:5000/news</code></br>

<p> 5. Perfect! the server and client are running with Docker! 😊</p>


<h2>Steps if ypu are using ypur own OSOperating System (OS)</h2>
<p> 1. Clone the repository in your local machine<p></br>
<p> 2. Open the project in VS Code or your favorite ✨ code editor</p></br>
<p> 3. Open two terminals. One of them should enter into the folder 📁 <code>cd /client</code> </p> </br>
<p> 4. Run the next command, in both terminals:</p>
<p></p><code>npm install</code></br>

<p> 5. Close the terminal which is into "client" folder, and Run the command:</p> </br>
<p></p><code>npm run dev</code></br>

<p> 4. Open a browser and type the next urls 🐱‍💻:</p></br>
<p></p><code>http://localhost:3000/</code></br>
<p></p><code>http://localhost:5000/news</code></br>

<p> 5. Perfect! the server and client are running in your local machine! 😊</p>

